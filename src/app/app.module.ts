import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Views/login/login.component';
import { RegisterComponent } from './Views/register/register.component';
import { ManageCenterComponent } from './Views/manage-center/manage-center.component';
import { ManageCenterStoresComponent } from './Views/manage-center-stores/manage-center-stores.component';
import { TaskManagerComponent } from './Views/task-manager/task-manager.component';
import { BranchesComponent } from './Views/branches/branches.component';
import { BranchStoreComponent } from './Views/branch-store/branch-store.component';
import { CartaMenuComponent } from './Views/carta-menu/carta-menu.component';
import { CustomizeSidepanelComponent } from './Views/customize-sidepanel/customize-sidepanel.component';
import { CustomizeMaincontentComponent } from './Views/customize-maincontent/customize-maincontent.component';
import { CustomizeViewComponent } from './Views/customize-view/customize-view.component';
import { PlatosIngredientesModificadoresComponent } from './Views/platos-ingredientes-modificadores/platos-ingredientes-modificadores.component';
import { CreateQrComponent } from './Views/create-qr/create-qr.component';
import { CreateQrBranchComponent } from './Views/create-qr-branch/create-qr-branch.component';
import { CreateQrTableComponent } from './Views/create-qr-table/create-qr-table.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AddRestaurantComponent } from './Views/add-restaurant/add-restaurant.component';
import { RestaurantsComponent } from './Views/restaurants/restaurants.component';
import { AddPlateComponent } from './Views/add-plate/add-plate.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ManageCenterComponent,
    ManageCenterStoresComponent,
    TaskManagerComponent,
    BranchesComponent,
    BranchStoreComponent,
    CartaMenuComponent,
    CustomizeSidepanelComponent,
    CustomizeMaincontentComponent,
    CustomizeViewComponent,
    PlatosIngredientesModificadoresComponent,
    CreateQrComponent,
    CreateQrBranchComponent,
    CreateQrTableComponent,
    AddRestaurantComponent,
    RestaurantsComponent,
    AddPlateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
