import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { SucursalDTO } from '../Models/sucursal-dto';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SucursalService {
  private readonly URL = environment.url_service_api;

  constructor(private http: HttpClient) { }

  AgregaSucursal(data: SucursalDTO) {
    const urlRegistro = this.URL + '/Sucursal';

    const headers = { 'Content-Type': 'application/json' };
    return this.http.post(urlRegistro, data, { headers });
  }

  EditarSucursal(data: SucursalDTO, IdSucursal: number) {
    const urlRegistro = this.URL + '/EditarSucursal/' + IdSucursal;

    const headers = { 'Content-Type': 'application/json' };
    return this.http.post(urlRegistro, data, { headers });
  }

  EliminarSucursal(IdSucursal: number) {
    const urlRegistro = this.URL + '/EliminarSucursal/' + IdSucursal;

    const headers = { 'Content-Type': 'application/json' };
    return this.http.get(urlRegistro);
  }

  ListaSucursales() {
    const urlRegistro = this.URL + '/Sucursal';

    const headers = { 'Content-Type': 'application/json' };
    return this.http.get(urlRegistro);
  }
}
