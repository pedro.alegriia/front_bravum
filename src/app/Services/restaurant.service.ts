import { Injectable } from '@angular/core';
import { RestauranteDTO } from '../Models/restaurante-dto';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
// Interfces
import { CartaMenu } from '../interfaces/CartaMenu';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  private readonly urlApi = environment.url_service_api;

  constructor(private http: HttpClient) { }

  AgregaRestaurante(data: RestauranteDTO) {
    const urlRegistro = this.urlApi + '/Restaurant';

    const headers = { 'Content-Type': 'application/json' };
    return this.http.post(urlRegistro, data, { headers });
  }

  ListaRestaurantes() {
    let urlRegistro = this.urlApi + '/Restaurant';
    const headers = { 'Content-Type': 'application/json' };
    return this.http.get(urlRegistro);
  }

  EditarRestaurant(data : RestauranteDTO, IdComercio:number) {
    let urlRegistro = this.urlApi + '/EditarRestaurant/' + IdComercio;

    const headers = { 'Content-Type': 'application/json' };
    return this.http.post(urlRegistro, data, { headers });
  }

  EliminarRestaurante(IdComercio:number) {
    let urlRegistro = this.urlApi + '/EliminarRestaurant/' + IdComercio;

    const headers = { 'Content-Type': 'application/json' };
    return this.http.get(urlRegistro);
  }

  public getMenus(idComercio?: number): Observable<CartaMenu[]> {
    if (!idComercio) idComercio = 1;
    return this.http.get<CartaMenu[]>(this.urlApi + '/CartaMenu/' + idComercio);
  }

  addMenu(menu: CartaMenu, idComercio?: number){
    if (!idComercio) idComercio = 1;
    return this.http.post(this.urlApi + '/CartaMenu/' + idComercio, menu);
  }

  editMenu(menu: CartaMenu, idComercio?: number){
    if (!idComercio) idComercio = 1;
    return this.http.post(this.urlApi + '/CartaMenu/' + idComercio + '/' + menu.IdCarta, menu);
  }

  deleteMenu(menu: CartaMenu){
    const params = {IdCarta: menu.IdCarta};
    return this.http.post(this.urlApi + '/CartaMenu/' + menu.IdCarta, params);
  }
}
