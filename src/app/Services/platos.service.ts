import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
// Interfaces
import { Modificador } from '../interfaces/Modificador';
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class PlatosService {
  private readonly urlApi = environment.url_service_api;

  constructor(private http: HttpClient) { }

  public getModifiers(): Observable<Modificador[]> {

    return this.http.get<Modificador[]>(this.urlApi + '/Modificadores/1');
  }

  addModifier(modifier: Modificador){
    return this.http.post(this.urlApi + '/Modificadores', modifier);
  }

  editModifier(modifier: Modificador){
    return this.http.post(this.urlApi + '/Modificadores/' + modifier.IdModificador, modifier);
  }

  deleteModifier(modifier: Modificador){
    const params = {IdModificador: modifier.IdModificador};
    return this.http.post(this.urlApi + '/Modificadores/' + modifier.IdModificador, params);
  }

}
