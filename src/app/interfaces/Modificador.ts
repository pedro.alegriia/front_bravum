import {ModificadorItem} from './ModificadorItem';
export interface Modificador {
    IdModificador: number;
    Nombre: string;
    SeleccionMultiple: boolean;
    SeleccionUnica: boolean;
    Minimo: number;
    Maximo: number;
    Precio: number;
    Elementos: ModificadorItem[];
}