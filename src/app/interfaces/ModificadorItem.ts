
export interface ModificadorItem {
    IdElemento: number;
    Nombre: string;
    Precio: boolean;
    ValorAdicional: boolean;
    Costo: number;
    Minimo: number;
    Maximo: number;
    Estatus: boolean;
}