export interface CartaMenu {
    IdCarta?: number;
    NombreRestaurant: string;
    FuenteNombre: string;
    DescripcionRestaurant: string;
    FuenteDescripcion: string;
    ImagenPrincipal: string;
    ImagenCabecera: string;
    ColorCabecera: string;
    Secciones?: Seccion[];
    Platos?: Plato[];
}

interface Seccion {
    IdSeccion?: number;
    NombreSeccion: string;
    ColorSeccion: string;
    Platos?: Plato[];
}

interface Plato{
    IdPlato?: number;
    IDComercio: number;
    Nombre: string;
    Descripcion: string;
    NombredeFantasiaInterno: string;
    Orden: number;
}
