import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCenterStoresComponent } from './manage-center-stores.component';

describe('ManageCenterStoresComponent', () => {
  let component: ManageCenterStoresComponent;
  let fixture: ComponentFixture<ManageCenterStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCenterStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCenterStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
