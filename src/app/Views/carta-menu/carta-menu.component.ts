import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
// Service
import {RestaurantService} from '../../Services/restaurant.service';
// Interfaces
import {CartaMenu} from '../../interfaces/CartaMenu';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NotificationService} from '../../Services/notification.service';

@Component({
  selector: 'app-carta-menu',
  templateUrl: './carta-menu.component.html',
  styleUrls: ['./carta-menu.component.css']
})
export class CartaMenuComponent implements OnInit {
  menusList: CartaMenu[];

  mdlMenu: CartaMenu;
  @ViewChild('modalAgregarMenu') modal: TemplateRef<any>; // Note: TemplateRef

  constructor(private servRestorant: RestaurantService, private router: Router,
              private modalService: NgbModal, private notify: NotificationService
  ) {
    this.resetModelMenu();
  }

  ngOnInit(): void {
    this.loadMenus();
  }

  AgregarMenu(): void {
    this.mdlMenu.Secciones = [];
    if (this.mdlMenu.NombreRestaurant !=''){
      this.servRestorant.addMenu(this.mdlMenu).subscribe(status => {
        if (status){
          this.notify.showSuccess('La sucursal se ha creado con éxito.', 'Restaurant Creado.');
          this.modalService.dismissAll(this.modal);
          this.loadMenus();
        }else{
          this.notify.showError('Ha surgido un error, intente más tarde.', 'Error.');
        }
      });
    }else {
      this.notify.showError('El Nombre del menú es requerido', 'Informacion');
    }

  }

  loadMenus(){
    this.servRestorant.getMenus().subscribe((menus: CartaMenu[]) => {
      if (menus){
        this.menusList = menus;
      }
    }, error => {
      console.log(error);
    });
  }

  onSelectFile(event, type: string) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let imgB64 = event.target.result.toString();
        if (type == 'main'){
          this.mdlMenu.ImagenPrincipal = imgB64;
        }else {
          this.mdlMenu.ImagenCabecera = imgB64;
        }

      }
    }
  }

  adminMenu(menu: CartaMenu){
    localStorage.setItem('menu', JSON.stringify(menu));
    this.router.navigate(['customize-sidepanel']);
  }

  ModalAgregar(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  resetModelMenu(){
    this.mdlMenu = {
      NombreRestaurant: '',
      FuenteNombre: '',
      DescripcionRestaurant: '',
      FuenteDescripcion: '',
      ImagenPrincipal: '',
      ImagenCabecera: '',
      ColorCabecera: '',
    };
  }

  hasItemsSeccion(ob, index: string, level: number, subindex?: string) {
    let itemsqtt: number = 0;
    if (level == 1) {

      if (ob.hasOwnProperty(index)) {
        itemsqtt = ob[index].length;
      }
    }else if (level == 2){
      if (ob.hasOwnProperty(index)) {
        if (ob[index].length > 0){
          for (let i of ob[index]){
            if (i.hasOwnProperty(subindex)){
              itemsqtt += i[subindex].length;
            }
          }
        }

      }
    }
    return itemsqtt;
  }

}
