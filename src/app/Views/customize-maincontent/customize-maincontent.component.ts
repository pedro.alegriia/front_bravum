import { Component, ViewChild, TemplateRef, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {CartaMenu} from '../../interfaces/CartaMenu';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customize-maincontent',
  templateUrl: './customize-maincontent.component.html',
  styleUrls: ['./customize-maincontent.component.css']
})
export class CustomizeMaincontentComponent implements OnInit {
  @ViewChild('modalAsigPlates') modalAsigPlates: TemplateRef<any>; // Note: TemplateRef
  @Output() _saveAndPublish = new EventEmitter<string>();
  @Output() selecteSectionToDrop = new EventEmitter<string>();
  @Input() platesToDrop: any;
  @Input() menu: CartaMenu;

  statusDropIn: boolean = false;
  dropInSectionSelected: string;

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  saveAndPublish(){
    this._saveAndPublish.emit('click');
  }

  dropInSection(section){
  this.selecteSectionToDrop.emit(section.IdSeccion);
    this.statusDropIn = true;

    this.dropInSectionSelected = section.NombreSeccion;

    setTimeout( () => {
      this.statusDropIn = false;
      this.modalService.dismissAll(this.modalAsigPlates);
    }, 4000);
  }

  getQttPlatesToDrop(){
    return this.platesToDrop.length;
  }

}
