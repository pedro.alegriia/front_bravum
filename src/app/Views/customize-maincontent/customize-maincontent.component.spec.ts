import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeMaincontentComponent } from './customize-maincontent.component';

describe('CustomizeMaincontentComponent', () => {
  let component: CustomizeMaincontentComponent;
  let fixture: ComponentFixture<CustomizeMaincontentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomizeMaincontentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizeMaincontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
