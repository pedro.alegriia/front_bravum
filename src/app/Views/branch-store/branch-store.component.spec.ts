import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchStoreComponent } from './branch-store.component';

describe('BranchStoreComponent', () => {
  let component: BranchStoreComponent;
  let fixture: ComponentFixture<BranchStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BranchStoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
