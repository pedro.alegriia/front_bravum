import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SucursalDTO } from 'src/app/Models/sucursal-dto';
import { NotificationService } from 'src/app/Services/notification.service';
import { SucursalService } from 'src/app/Services/sucursal.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {
  Id: number = 0;
  IDTipoComercio: number = 0;
  IdComercio: number = 0;
  Nombre: string = '';
  Correo: string = '';
  dataModel = new SucursalDTO(this.IdComercio, '', 0, 0, '', '', 0, 0, 0, 0, false, false, false, false, false, '', '');
  listRestaurant: any;
  IdSucursal: number;
  urlImg: string = '';

  // MODALES
  @ViewChild('AgregarRestaurante') editModal: TemplateRef<any>; // Note: TemplateRef
  @ViewChild('EditarSucursal') editar: TemplateRef<any>; // Note: TemplateRef

  constructor(
      private router: Router, private modalService: NgbModal,
      public resService: SucursalService,
      private notifyService: NotificationService
  ) {
    const navigation = this.router.getCurrentNavigation();
    let information = navigation.extras as {
      IdComercio: number,
      IDTipoComercio: number,
      IdPais: number,
      TipoVAT: number,
      NumeroVAT: number,
      NombreComercio: string,
      MarcaComercial: string,
      Logo: string,
      AtiendePublico: number,
      GiroComercial: string,
      Lema: string,
      EstadoEnPlataforma: number,
      MensajeEnCatalogo: number,
      AvisoEnCatalogos: string,
      Venta: number
    };
    this.Nombre = information.NombreComercio;
    this.IDTipoComercio = information.IDTipoComercio;
    this.IdComercio = information.IdComercio;
  }

  ngOnInit(): void {
    this.resService.ListaSucursales().subscribe(data => {
      this.listRestaurant = data;
    });
  }

  ModalAgregarSucursal(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  Editar(data: any) {
    this.modalService.open(this.editar, { scrollable: true });
    this.dataModel = new SucursalDTO(
        data['IdSucursal'],
        data['NombreSucursal'],
        data['Aforo'],
        data['Mesas'],
        data['Horario_Funcionamiento'],
        data['Horario_Funcionamiento_Fin'],
        data['Reparticion_Propina'],
        data['Estado_En_La_Plataforma'],
        data['Delivery'],
        1,
        data['Estacionamiento_Propio'],
        data['Terraza'],
        data['Pet_Friendly'],
        data['Dietas_Especiales'],
        data['Medios_De_Pago'],
        data['Imagen'],
        data['ImagenxSucursal']
    );
    this.IdSucursal = data['IdSucursal'];
  }

  EditarSucursales(){
    this.resService.EditarSucursal(this.dataModel, this.IdSucursal).subscribe(data => {
      if(data){
        this.notifyService.showSuccess('La sucursal se ha editado con éxito.', 'Restaurant Creado.');
        this.modalService.dismissAll(this.editModal);
        this.resService.ListaSucursales().subscribe(data => {
          this.listRestaurant = data;
        });
      }else{
        this.notifyService.showError('Ha surgido un error, intente más tarde.', 'Error.');
      }
    });
  }

  EliminarSucursal(data:any){
    Swal.fire({
      title: '¿Seguro que desea eliminar la sucursal?',
      html: 'El restaurant <strong>' + data['NombreSucursal'] + '</strong> será eliminado de tu lista!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, conservar!'
    }).then((result) => {
      if (result.value) {
        this.resService.EliminarSucursal(data['IdSucursal']).subscribe(data => {
          if(data){
            this.notifyService.showSuccess('La sucursal se ha eliminó con éxito.', 'Sucursal Eliminado.');
            this.resService.ListaSucursales().subscribe(data => {
              this.listRestaurant = data;
            });
          }else{
            this.notifyService.showError('Ha surgido un error, intente más tarde.', 'Error.');
          }
        });
        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  AgregarRestaurant(): void{
    this.dataModel.IdComercio = this.IdComercio;
    this.dataModel.ImagenxSucursal = this.urlImg;
    this.resService.AgregaSucursal(this.dataModel).subscribe(data => {
      if(data){
        this.notifyService.showSuccess('La sucursal se ha creado con éxito.', 'Restaurant Creado.');
        this.modalService.dismissAll(this.editModal);
        this.resService.ListaSucursales().subscribe(data => {
          this.listRestaurant = data;
        });
      }else{
        this.notifyService.showError('Ha surgido un error, intente más tarde.', 'Error.');
      }
    });
  }

  Administrar(data: any){
    // this.router.navigate(["admonrestaurant"], data);
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.urlImg = event.target.result.toString();
      }
    }
  }
}
