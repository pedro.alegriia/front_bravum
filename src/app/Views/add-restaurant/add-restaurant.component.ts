import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.css']
})
export class AddRestaurantComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  saveRestaurant(){
    this.router.navigate(['restaurants']);
  }
}
