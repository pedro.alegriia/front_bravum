import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-plate',
  templateUrl: './add-plate.component.html',
  styleUrls: ['./add-plate.component.css']
})
export class AddPlateComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  savePlate(){
    this.router.navigate(['platos-ingredientes-modificadores']);
  }

}
