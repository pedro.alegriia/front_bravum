import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQrBranchComponent } from './create-qr-branch.component';

describe('CreateQrBranchComponent', () => {
  let component: CreateQrBranchComponent;
  let fixture: ComponentFixture<CreateQrBranchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateQrBranchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQrBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
