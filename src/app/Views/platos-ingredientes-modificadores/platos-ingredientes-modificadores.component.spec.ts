import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatosIngredientesModificadoresComponent } from './platos-ingredientes-modificadores.component';

describe('PlatosIngredientesModificadoresComponent', () => {
  let component: PlatosIngredientesModificadoresComponent;
  let fixture: ComponentFixture<PlatosIngredientesModificadoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatosIngredientesModificadoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatosIngredientesModificadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
