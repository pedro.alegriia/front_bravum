import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
// Services
import {PlatosService} from '../../Services/platos.service';
import {NotificationService} from '../../Services/notification.service';
// Intefaces
import {Modificador} from '../../interfaces/Modificador';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-platos-ingredientes-modificadores',
  templateUrl: './platos-ingredientes-modificadores.component.html',
  styleUrls: ['./platos-ingredientes-modificadores.component.css']
})
export class PlatosIngredientesModificadoresComponent implements OnInit {
  modifiersList: Modificador[];
  modifierNew: Modificador;
  actionAdd: boolean = false;
  actionEdit: boolean = false;

  constructor(private router: Router, private pltService: PlatosService, private notify: NotificationService) {
    this.setDefaultModier();
  }

  ngOnInit(): void {
    this.loadModifiers();
  }

  /* addItemModifier(index){
   let hmtlnode: any = document.getElementById(index);
   let newItemName = hmtlnode.value;
   if (newItemName && newItemName.length>0){
   let idMod = index.split('-')[1];
   this.modifiersList.filter(mod => {
   if (mod.IdModificador == idMod){

   mod.Elementos.push({
   IdElemento: 0,
   Nombre: newItemName,
   Precio: true,
   ValorAdicional: false,
   Costo: 10,
   Minimo: 10,
   Maximo: 20,
   Estatus: true
   });

   }
   });
   }

   }*/

  AddModifier(){
    let isValid = false;
    let errors: number = 0;
    let errorMesages: string='';

    if (this.modifierNew.Nombre != '' && this.modifierNew.Precio != null && (this.modifierNew.SeleccionMultiple || this.modifierNew.SeleccionUnica)){
      if (this.modifierNew.Elementos.length > 0 ){
        for ( let index in this.modifierNew.Elementos){
          if (this.modifierNew.Elementos[index].Costo > 0 && (this.modifierNew.Elementos[index].Precio || this.modifierNew.Elementos[index].ValorAdicional)){

          } else {
            errors += 1;
            errorMesages = 'Marque una opcion e Ingrese el precio';
          }
        }
      }else {
        errors += 1;
        errorMesages = 'Agrege almenos un elemento';
      }

    }else {
      errors += 1;
      errorMesages = 'Ingrese los datos del modificador (Nombre,Tipo seleccion, min, max, Precio)';
    }

    if ( errors == 0 ) isValid =  true;

    if (isValid) {
      if (this.actionEdit) {

        this.pltService.editModifier(this.modifierNew).subscribe(res => {
          if (res) {
            this.showAlert('Agregado', 'succes');
            this.loadModifiers();
            this.actionAdd = false;
            this.actionEdit = false;
            this.setDefaultModier();
          } else {
            this.showAlert('Ocurrio un eeror al guardar', 'error');
          }
        }, error => {
          console.log(error);
        });

      } else {
        this.modifierNew.IdModificador = null;

        this.pltService.addModifier(this.modifierNew).subscribe(res => {
          if (res) {
            this.showAlert('Agregado', 'success');
            this.loadModifiers();
            this.actionAdd = false;
            this.setDefaultModier();
          } else {
            this.showAlert('Ocurrio un eeror al guardar', 'error');
          }
        }, error => {
          console.log(error);
        });
      }
    }else {
      this.showAlert(errorMesages, 'error');
    }
  }

  editModifier(mod: Modificador){
    this.setDefaultModier();
    this.actionAdd = true;
    this.actionEdit = true;
    this.modifierNew = Object.assign({}, mod);
  }

  addTempItems(){
    let htmlEl: any = document.getElementById('nameTemoItem');
    let newItemName = htmlEl.value;
    if (newItemName && newItemName.length > 0){
      let items = Object.assign([], this.modifierNew.Elementos);
      items.push({
        IdElemento: 0,
        Nombre: newItemName,
        Precio: null,
        ValorAdicional: null,
        Costo: 0,
        Minimo: 0,
        Maximo: 0,
        Estatus: true
      });
      this.modifierNew.Elementos = Object.assign([], items);
      htmlEl.value = '';
    }
  }

  removeItemTem(index){
    this.modifierNew.Elementos.splice(index, 1);
  }

  private loadModifiers(){
    this.pltService.getModifiers().subscribe((res: Modificador[]) => {
      if (res){
        this.modifiersList = res;
      }
    }, error => {
      console.log(error);
    });
  }

  private setDefaultModier(){
    this.modifierNew = {
      IdModificador: 0,
      Nombre: '',
      SeleccionMultiple: null,
      SeleccionUnica: null,
      Minimo: null,
      Maximo: null,
      Precio: null,
      Elementos: [],
    };
  }

  ceancel(){
    this.setDefaultModier();
    this.actionAdd = false;
    this.actionEdit = false;
  }

  addGroup(){
    this.actionAdd = true;
  }


  linkAddPlate(){
    this.router.navigate(['add-plate']);
  }

  CheckBoxGroup(input, section, index?: number){
    if (section == 'form'){
      let value = input.value;
      if (value == 'unic'){
        this.modifierNew.SeleccionUnica = false;
        this.modifierNew.SeleccionMultiple = true;
      }else{
        this.modifierNew.SeleccionUnica = true;
        this.modifierNew.SeleccionMultiple = false;
      }
    }else {

      if (index != undefined ){
        let value = input.value;;
        for (let iE in this.modifierNew.Elementos){

          if (parseInt(iE) == index){
            if (value == 'price'){
              this.modifierNew.Elementos[iE].Precio = true;
              this.modifierNew.Elementos[iE].ValorAdicional = false;
            }else{
              this.modifierNew.Elementos[iE].Precio = false;
              this.modifierNew.Elementos[iE].ValorAdicional = true;
            }

          }
        }
      }
    }

  }

  changePiceItem(input, index?: number){
    if (index != undefined ){
      const value = input.value;
      for (let iE in this.modifierNew.Elementos){

        if (parseInt(iE) == index){
          this.modifierNew.Elementos[iE].Costo = value;
        }
      }
    }
  }

  showAlert(msg, type){
    let params = {
      title: msg,
      icon: type,
    };
    Swal.fire(params).then((result) => {

    });
  }
}
