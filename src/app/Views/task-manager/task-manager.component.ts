import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.css']
})
export class TaskManagerComponent implements OnInit {
  NombreComercio: string;
  MarcaComercial: string;
  Lema :string;
  Direccion:string;

  dataRestaurant : any;
  constructor(private router : Router) { 
    const navigation = this.router.getCurrentNavigation();
    let information = navigation.extras as {
      IdComercio:number,
      IDTipoComercio: number,
      IdPais: number,
      TipoVAT: number,
      NumeroVAT: number,
      NombreComercio: string,
      MarcaComercial: string,
      Logo: string,
      AtiendePublico: number,
      GiroComercial: string,
      Lema: string,
      EstadoEnPlataforma: number,
      MensajeEnCatalogo: number,
      AvisoEnCatalogos: string,
      Venta: number,
      Direcion: string,
      Correo: string,
      TelefonoFijo: string,
      TelefonoMovil:string
    };

    this.NombreComercio = information.NombreComercio;
    this.MarcaComercial = information.NombreComercio;
    this.Lema = information.Lema;
    this.Direccion = information.Direcion;

    this.dataRestaurant = information;
  }

  ngOnInit(): void {
  }

  PlatosIngredientes(){
    this.router.navigate(["platos-ingredientes-modificadores"]);
  }

  Sucursales(){
    this.router.navigate(["/branches"], this.dataRestaurant);
  }
  createQr(){
    this.router.navigate(['create-qr']);
  }
}
