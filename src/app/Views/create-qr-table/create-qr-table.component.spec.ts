import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQrTableComponent } from './create-qr-table.component';

describe('CreateQrTableComponent', () => {
  let component: CreateQrTableComponent;
  let fixture: ComponentFixture<CreateQrTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateQrTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQrTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
