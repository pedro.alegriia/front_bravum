import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { RestauranteDTO } from '../../Models/restaurante-dto';
import { UserDTO } from '../../Models/user-dto';
import { RestaurantService } from '../../Services/restaurant.service';
import { NotificationService } from '../../Services/notification.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-manage-center',
  templateUrl: './manage-center.component.html',
  styleUrls: ['./manage-center.component.css']
})
export class ManageCenterComponent implements OnInit {
  isShow = false;
  urlImg: string = "";
  Id: number = 0;
  Nombre : string = "";
  Correo : string = "";
  dataModel = new RestauranteDTO(0, 0, 0, 0, "", "", this.urlImg, 0, "", "", 0, 0, "", 0, "", "", "", "");
  listRestaurant : any;
  IdComercio : number;
  @ViewChild('AgregarRestaurante') editModal : TemplateRef<any>; // Note: TemplateRef
  @ViewChild('EditaRestaurante') editar : TemplateRef<any>; // Note: TemplateRef

  constructor(private router : Router, private modalService: NgbModal, public resService : RestaurantService, private notifyService : NotificationService) {
    this.Id = 0;
    const navigation = this.router.getCurrentNavigation();
    let information = navigation.extras as {
      Correo: string,
      Id: number,
      Nombre: string
    };

    
    this.Id = information.Id;
    this.Nombre = information.Nombre;
    this.Correo = information.Correo;

    
   }

  userInformation = new UserDTO(this.Id, this.Nombre, this.Correo);
  
  ngOnInit(): void {
    if(this.Id == 0){
      this.router.navigate(["login"]);
    }else{
      this.resService.ListaRestaurantes().subscribe(data => {
        this.listRestaurant = data;
      });
    }

    
  }

  AgregarRestaurantModal(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  Editar(data: any) {
    this.IdComercio = data["IdComercio"];
    this.modalService.open(this.editar, { scrollable: true });
    this.dataModel = new RestauranteDTO(data["IdComercio"], data["IdPais"], data["TipoVAT"], data["NumeroVAT"], data["NombreComercio"],
    data["MarcaComercial"], data["Logo"], data["AtiendePúblico"], data["GiroComercial"], data["Lema"], data["EstadoEnPlataforma"],
    data["MensajeEnCatalogo"], data["AvisoEnCatálogos"], data["Venta"], data["Direcion"], 
    data["Correo"], data["TelefonoFijo"], data["TelefonoMovil"]);   
  }

  EditarRestaurant(){
    this.resService.EditarRestaurant(this.dataModel, this.IdComercio).subscribe(data => {
      if(data){
        this.notifyService.showSuccess("El restaurant se ha modificado con éxito.", "Restaurant Creado.");
        this.modalService.dismissAll(this.editModal);
        this.resService.ListaRestaurantes().subscribe(data => {
          this.listRestaurant = data;
        });
      }else{
        this.notifyService.showError("Ha surgido un error, intente más tarde.", "Error.");
      }
    });
  }

  EliminarRestaurante(data:any){
    Swal.fire({
      title: '¿Seguro que desea eliminar el restaurant?',
      html: 'El restaurant <strong>' + data["NombreComercio"] + '</strong> será eliminado de tu lista!',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, conservar!'
    }).then((result) => {
      if (result.value) {
        this.resService.EliminarRestaurante(data["IdComercio"]).subscribe(data => {
          if(data){
            this.notifyService.showSuccess("El restaurant se ha eliminó con éxito.", "Restaurant Eliminado.");
            this.resService.ListaRestaurantes().subscribe(data => {
              this.listRestaurant = data;
            });
          }else{
            this.notifyService.showError("Ha surgido un error, intente más tarde.", "Error.");
          }
        });
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        
      }
    })
  }

  AgregarRestaurant(){
    this.dataModel.Logo = this.urlImg;
    this.resService.AgregaRestaurante(this.dataModel).subscribe(data => {
      if(data){
        this.notifyService.showSuccess("El restaurant se ha creado con éxito.", "Restaurant Creado.");
        this.modalService.dismissAll(this.editModal);
        this.resService.ListaRestaurantes().subscribe(data => {
          this.listRestaurant = data;
        });
      }else{
        this.notifyService.showError("Ha surgido un error, intente más tarde.", "Error.");
      }
    });
  }

  Administrar(data : any){
    this.router.navigate(["task-manager"], data);
  }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.urlImg = event.target.result.toString();
      }
    }
}

}
