import { Component, OnInit } from '@angular/core';
import {CartaMenu} from '../../interfaces/CartaMenu';
import {RestaurantService} from '../../Services/restaurant.service';
import {NotificationService} from '../../Services/notification.service';

@Component({
  selector: 'app-customize-sidepanel',
  templateUrl: './customize-sidepanel.component.html',
  styleUrls: ['./customize-sidepanel.component.css']
})
export class CustomizeSidepanelComponent implements OnInit {
  menuInfo: CartaMenu;
  actionEdit: boolean;

  platesList: any = [];
  platesFIlterList: any = [];

  constructor(private restService: RestaurantService, private notify: NotificationService) {
    this.actionEdit = false;
    let storItem = localStorage.getItem('menu');
    if (storItem) {
      this.menuInfo = JSON.parse(storItem);
      this.actionEdit = true;
      this.getPlatos();

      // Test Only
      /*
      this.menuInfo.Secciones.push({
        NombreSeccion: "sssss",
        ColorSeccion: "string",
        Platos: [
          {
            IdPlato: 2,
            IDComercio: 3,
            Nombre: "Sandwitch",
            Descripcion: "string",
            NombredeFantasiaInterno: "string",
            Orden: 1
          }]
      });
      */
    }
  }

  ngOnInit(): void {
  }

  saveAndPublish($event){
    this.restService.editMenu(this.menuInfo).subscribe(res => {
      if (res) {
        this.notify.showSuccess('Agregado...', 'Menu');

        this.actionEdit = false;
        this.setDefaultMenu();
      } else {
        this.notify.showError('Ocurrio un eeror al guardar', 'Error');
      }
    }, error => {
      console.log(error);
    });
    console.log(this.menuInfo);
  }

  expandedSectionPanel: string;
  xpandItemSection(index){
    this.expandedSectionPanel = index;
  }

  addItemSectionSidepanel(){
    this.menuInfo.Secciones.push({
      NombreSeccion: 'New',
      ColorSeccion: '',
      Platos: []
    });
  }

  removeSectSidePanel(index){
    this.menuInfo.Secciones.splice(index, 1);
    console.log(this.menuInfo);
  }

  selectedPlatesPanelSide: any = [];
  checkPlate(indexPlate){
    if (this.selectedPlatesPanelSide.includes(indexPlate)){
      const _index: any = this.selectedPlatesPanelSide.findIndex(index => index === indexPlate);
      this.selectedPlatesPanelSide.splice(_index, 1);
    }else {
      this.selectedPlatesPanelSide.push(indexPlate);
    }
  }

  platesToDrop: any = [];
  platosDrag(){
    this.platesToDrop = [];
    // Buscar elementos para despues asignarlo a alguna seccion
    if (this.selectedPlatesPanelSide.length > 0){
      for (let i of this.selectedPlatesPanelSide){
        const item: any = Object.assign({}, this.platesFIlterList[i]);
        this.platesToDrop.push(item);
      }
    }
  }

  platesDropInSection(idSection){
    this.menuInfo.Secciones.filter(section => {
      if (section.IdSeccion == idSection){
        if (!section.hasOwnProperty('Platos')){
          section['Platos'] = [];
        }
        section.Platos.push(this.platesToDrop);
      }
    });
  }

  // Reset object or redirect to carta-menu
  setDefaultMenu(){
  }

  filterInPlates(input){
    this.platesFIlterList = [];
    const filter = input.value;
    const plates = Object.assign([], this.platesList);
    if (filter != ''){
      if ( plates.length>0){
        for (const p of plates) { console.log(p['Nombre'].toLowerCase(), filter.toLowerCase());
          if (p['Nombre'].toLowerCase().includes(filter.toLowerCase()) ) {
            const pp = Object.assign({}, p);
            this.platesFIlterList.push(pp);
          }

        }
      }
    } else {
      this.platesFIlterList = Object.assign([], plates);
    }

  }

  // esta lista que devuelve la funcion deveria venir de un un endpoint separado
  getPlatos(){
    let items: any = [];
    for (let sec of this.menuInfo.Secciones){
      if (sec.hasOwnProperty('Platos')) {
        for (let iu of sec['Platos']){
          const p = Object.assign({}, iu);
          this.platesList.push(p);
        }
      }
    }
    this.platesFIlterList = Object.assign([], this.platesList);
  }

}
