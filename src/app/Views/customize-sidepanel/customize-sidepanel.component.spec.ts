import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeSidepanelComponent } from './customize-sidepanel.component';

describe('CustomizeSidepanelComponent', () => {
  let component: CustomizeSidepanelComponent;
  let fixture: ComponentFixture<CustomizeSidepanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomizeSidepanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizeSidepanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
