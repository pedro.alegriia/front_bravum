export class SucursalDTO {
    constructor(
        public IdComercio : number, 
        public NombreSucursal : string,
        public Aforo : number,
        public Mesas : number,
        public Horario_Funcionamiento : string,
        public Horario_Funcionamiento_Fin : string,
        public Reparticion_Propina : number,
        public Estado_En_La_Plataforma : number,
        public Delivery : number,
        public Take_Away : number,
        public Estacionamiento_Propio : boolean,
        public Terraza : boolean,
        public Pet_Friendly : boolean,
        public Dietas_Especiales : boolean,
        public Medios_De_Pago : boolean,
        public Imagen : string,
        public ImagenxSucursal : string,
    ){}
}
