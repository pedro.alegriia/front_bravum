export class RestauranteDTO {
    constructor(
        public IDTipoComercio: number,
        public IdPais: number,
        public TipoVAT: number,
        public NumeroVAT: number,
        public NombreComercio: string,
        public MarcaComercial: string,
        public Logo: string,
        public AtiendePublico: number,
        public GiroComercial: string,
        public Lema: string,
        public EstadoEnPlataforma: number,
        public MensajeEnCatalogo: number,
        public AvisoEnCatalogos: string,
        public Venta: number,
        public Direcion: string,
        public Correo: string,
        public TelefonoFijo: string,
        public TelefonoMovil:string
    ){}
}
