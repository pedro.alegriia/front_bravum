import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Views/login/login.component';
import { RegisterComponent } from './Views/register/register.component';
import { ManageCenterComponent } from './Views/manage-center/manage-center.component';
import { ManageCenterStoresComponent } from './Views/manage-center-stores/manage-center-stores.component';
import { TaskManagerComponent } from './Views/task-manager/task-manager.component';
import { BranchesComponent } from './Views/branches/branches.component';
import { BranchStoreComponent } from './Views/branch-store/branch-store.component';
import { CartaMenuComponent } from './Views/carta-menu/carta-menu.component';
import { CustomizeSidepanelComponent } from './Views/customize-sidepanel/customize-sidepanel.component';
import { CustomizeMaincontentComponent } from './Views/customize-maincontent/customize-maincontent.component';
import { CustomizeViewComponent } from './Views/customize-view/customize-view.component';
import { PlatosIngredientesModificadoresComponent } from './Views/platos-ingredientes-modificadores/platos-ingredientes-modificadores.component';
import { CreateQrComponent } from './Views/create-qr/create-qr.component';
import { CreateQrBranchComponent } from './Views/create-qr-branch/create-qr-branch.component';
import { CreateQrTableComponent } from './Views/create-qr-table/create-qr-table.component';
import { AddRestaurantComponent } from './Views/add-restaurant/add-restaurant.component';
import { RestaurantsComponent } from './Views/restaurants/restaurants.component';
import {AddPlateComponent} from './Views/add-plate/add-plate.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'manage-center', component: ManageCenterComponent},
    {path: 'restaurants', component: ManageCenterStoresComponent},
    {path: 'task-manager', component: TaskManagerComponent},
    {path: 'branches', component: BranchesComponent},
    {path: 'branch-store', component: BranchStoreComponent},
    {path: 'carta-menu', component: CartaMenuComponent},
    {path: 'customize-sidepanel', component: CustomizeSidepanelComponent},
    {path: 'customize-maincontent', component: CustomizeMaincontentComponent},
    {path: 'customize-view', component: CustomizeViewComponent},
    {path: 'platos-ingredientes-modificadores', component: PlatosIngredientesModificadoresComponent},
    {path: 'add-plate', component: AddPlateComponent},
    {path: 'create-qr', component: CreateQrComponent},
    {path: 'create-qr-branch', component: CreateQrBranchComponent},
    {path: 'create-qr-table', component: CreateQrTableComponent},
    {path: 'add-restaurant', component: AddRestaurantComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
